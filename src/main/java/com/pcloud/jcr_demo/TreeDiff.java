package com.pcloud.jcr_demo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;

/**
 * Contains all differences between 2 JCR trees (nodes)
 * 
 * @author thetran
 *
 */
public class TreeDiff {
	/**
	 * Left tree
	 */
	private Node lhsNode;
	/**
	 * Right tree
	 */
	private Node rhsNode;
	/**
	 * Only in left hand side (lhs) tree
	 */
	private Set<String> lhsOnly = new HashSet<String>();
	/**
	 * Both on left hand side and right hand side
	 */
	private Map<String, NodeDiff> modified = new HashMap<String, NodeDiff>();
	/**
	 * Only in right hand side
	 */
	private Set<String> rhsOnly = new HashSet<String>();

	public Node getLhsNode() {
		return lhsNode;
	}

	public Node getRhsNode() {
		return rhsNode;
	}

	public Set<String> getLhsOnly() {
		return lhsOnly;
	}

	public Map<String, NodeDiff> getModified() {
		return modified;
	}

	public Set<String> getRhsOnly() {
		return rhsOnly;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean hasDiff = false;
		
		sb.append("\n==== DIFF SUMMARY ====\n");
		if (!lhsOnly.isEmpty()) {
			hasDiff = true;
			sb.append("++++ Only in left hand side:\n");
			sb.append(Arrays.toString(lhsOnly.toArray())).append("\n");
		}
		if (!rhsOnly.isEmpty()) {
			hasDiff = true;
			sb.append("---- Only in right hand side:\n");
			sb.append(Arrays.toString(rhsOnly.toArray())).append("\n");
		}
		if (!modified.isEmpty()) {
			hasDiff = true;
			sb.append("#### Modified:\n");
			for (String path : modified.keySet()) {
				sb.append("    " + path + " => " + modified.get(path) + "\n");
			}
		}
		if (!hasDiff) {
			sb.append("2 nodes are the same");
		}
		return sb.toString();
	}
}
