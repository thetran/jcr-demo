package com.pcloud.jcr_demo;

import org.apache.log4j.Logger;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

public class Utils {
    private static Logger logger = Logger.getLogger(Utils.class);

	public static boolean isEqual(Object obj1, Object obj2) {
		if (obj1 == null && obj2 == null) {
			return true;
		}
		if (obj1 != null) {
			return obj1.equals(obj2);
		} else {
			return obj2.equals(obj1);
		}
	}

    /** Recursively outputs the contents of the given node. */
    public static void dump(Node node) throws RepositoryException {
        // First output the node path
        logger.info(node.getPath());
        // Skip the virtual (and large!) jcr:system subtree
        if (node.getName().equals("jcr:system")) {
            return;
        }

        // Then output the properties
        PropertyIterator properties = node.getProperties();
        while (properties.hasNext()) {
            Property property = properties.nextProperty();
            if (property.getDefinition().isMultiple()) {
                // A multi-valued property, print all values
                Value[] values = property.getValues();
                for (int i = 0; i < values.length; i++) {
                    logger.info(
                            property.getPath() + " = " + values[i] .getString());
                }
            } else {
                // A single-valued property
                logger.info(
                        property.getPath() + " = " + property.getString());
            }
        }

        // Finally output all the child nodes recursively
        NodeIterator nodes = node.getNodes();
        while (nodes.hasNext()) {
            dump(nodes.nextNode());
        }
    }
    
    
    
    @SuppressWarnings("unused")
	public static void dump(Node node, List<String> paths) throws RepositoryException {
        // First output the node path
        logger.info(node.getPath());
        paths.add(node.getPath());
        
        // Skip the virtual (and large!) jcr:system subtree
        if (node.getName().equals("jcr:system")) {
            return;
        }

        // Then output the properties
		if (false) {
			PropertyIterator properties = node.getProperties();
			while (properties.hasNext()) {
				Property property = properties.nextProperty();
				if (property.getDefinition().isMultiple()) {
					// A multi-valued property, print all values
					Value[] values = property.getValues();
					for (int i = 0; i < values.length; i++) {
						logger.info(property.getPath() + " = "
								+ values[i].getString());
					}
				} else {
					// A single-valued property
					logger.info(property.getPath() + " = "
							+ property.getString());
				}
			}
		}

        // Finally output all the child nodes recursively
        NodeIterator nodes = node.getNodes();
        while (nodes.hasNext()) {
            dump(nodes.nextNode(), paths);
        }
    }
}
