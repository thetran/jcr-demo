package com.pcloud.jcr_demo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;

/**
 * Contains the differences between properties of 2 nodes.
 * 
 * @author thetran
 *
 */
public class NodeDiff {
	/**
	 * Left node
	 */
	private Node lhsNode;
	/**
	 * Right node
	 */
	private Node rhsNode;
	/**
	 * Properties only in left hand side node
	 */
	private Set<String> lhsOnly;
	/**
	 * Properties only in right hand side node
	 */
	private Set<String> rhsOnly;
	/**
	 * Contains all properties have been changed
	 */
	private Map<String, ValueChange> modified;

	public NodeDiff(Node lhsNode, Node rhsNode) {
		this.lhsNode = lhsNode;
		this.rhsNode = rhsNode;

		lhsOnly = new HashSet<String>();
		rhsOnly = new HashSet<String>();
		modified = new HashMap<String, ValueChange>();
	}

	public Node getLHSNode() {
		return lhsNode;
	}

	public Node getRHSNode() {
		return rhsNode;
	}

	public Set<String> getLhsProps() {
		return lhsOnly;
	}

	public Set<String> getRhsProps() {
		return rhsOnly;
	}

	public Map<String, ValueChange> getModifiedProps() {
		return modified;
	}

	public boolean hasChanges() {
		return !(lhsOnly.isEmpty() && rhsOnly.isEmpty() && modified.isEmpty());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NodeDiff [").append("lhsOnly=").append(lhsOnly)
				.append(", rhsOnly=").append(rhsOnly).append(", modified=")
				.append(modified).append("]");
		return builder.toString();
	}
}
