package com.pcloud.jcr_demo;

import javax.jcr.LoginException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;

import org.apache.jackrabbit.core.TransientRepository;
import org.apache.log4j.Logger;

import com.pcloud.jcr_demo.builder.TreeBuilder;
import com.pcloud.jcr_demo.builder.TreeData;

/**
 * Hello world!
 *
 */
public class App {
	public static final String T1 = "a";
	public static final String T2 = "b";
	public static Logger log = Logger.getLogger(App.class);

	public static void main(String[] args) throws LoginException, RepositoryException {
		Repository repository = new TransientRepository();
		

		try {
			TreeBuilder tb =  new TreeBuilder(repository);
			TreeData data1 = new TreeData(new String[] { "/a", "/a/a.2", "/a/a.1", "/z", "/z/z.2", "/z/z.1@p4=world", "/b", "/b/b.2@p1=10@p2=20@p3=333" });
			TreeData data2 = new TreeData(new String[] { "/z", "/z/z.1@p4=hello@p9=911", "/b", "/b/b.1@p5=hello_world" ,"/b/b.2@p1=30@p2=40"});
			
			tb.build(T1, data1);
			tb.build(T2, data2);
			
			DiffWorker dw = new DiffWorker(repository);
			TreeDiff td = dw.calculate(T1, T2);
			log.info(td);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
