package com.pcloud.jcr_demo;

import javax.jcr.Property;

/**
 * Store the left and right value of a properties.
 * 
 * @author thetran
 *
 */
public class ValueChange {
	/**
	 * The left hand side property
	 */
	private Property lhsProperty;
	/**
	 * The right hand side property
	 */
	private Property rhsProperty;
	/**
	 * Left hand side value
	 */
	private Object lhsValue;
	/**
	 * Right hand side value
	 */
	private Object rhsValue;


	public ValueChange(Property lhsProperty, Property rhsProperty, Object lhsValue, Object rhsValue) {
		this.lhsProperty = lhsProperty;
		this.rhsProperty = rhsProperty;
		this.lhsValue = lhsValue;
		this.rhsValue = rhsValue;
	}

	public Property getLhsProperty() {
		return lhsProperty;
	}

	public Property getRhsProperty() {
		return rhsProperty;
	}

	public Object getLhsValue() {
		return lhsValue;
	}

	public void setLhsValue(Object lhsValue) {
		this.lhsValue = lhsValue;
	}

	public Object getRhsValue() {
		return rhsValue;
	}

	public void setRhsValue(Object rhsValue) {
		this.rhsValue = rhsValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(lhsValue).append(" => ").append(rhsValue);
		return builder.toString();
	}
}
