/**
 * 
 */
package com.pcloud.jcr_demo.builder;

import java.util.Map;

import javax.jcr.Credentials;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import com.pcloud.jcr_demo.Utils;

/**
 * This class is responsible for constructing the data for a given workspace
 * 
 * @author thetran
 *
 */
public class TreeBuilder {
	private Credentials credentials = new SimpleCredentials("pcloud", "password".toCharArray());
	private Repository repository;
	
	public TreeBuilder(Repository repository) {
		super();
		this.repository = repository;
	}
	
	public void build(String ws, TreeData data) throws InvalidPathException, LoginException, NoSuchWorkspaceException, RepositoryException {
		createOrResetWorkspaceIfAny(ws);
		
		PathParser parser = new PathParser();
		Session session = repository.login(credentials, ws);
		
		try {
			Node root = session.getRootNode();
			
			for (String path : data.getPaths()) {
				NodeData nodeData = parser.parse(path);
				
				Node current = root;
				for (String nodeName : nodeData.getPaths()) {
					if (!current.hasNode(nodeName)) {
						current = current.addNode(nodeName);
					} else {
						current = current.getNode(nodeName);
					}
				}
				Map<String, String> props = nodeData.getProps();
				if (nodeData.getPaths().length > 0 && !props.isEmpty()) {
					for (String key : props.keySet()) {
						current.setProperty(key, props.get(key));
					}
				}
			}
			session.save();
			
		} finally {
			session.logout();
		}
	}
	
	private void createOrResetWorkspaceIfAny(String ws) throws LoginException, RepositoryException {
		Session session = null;
		try {
			session = repository.login(credentials, ws);
			Node root = session.getRootNode();
			
			// remove everything under the root node
			NodeIterator iter = root.getNodes();
			while (iter.hasNext()) {
				Node node = iter.nextNode();
				if (!"jcr:system".equals(node.getName())) {
					node.remove();
				}
			}
			
			session.save();
		} catch (NoSuchWorkspaceException e) {
			// create the new one
			if (session == null) {
				session = repository.login(credentials);
				session.getWorkspace().createWorkspace(ws);
			}
		} finally {
			if (session != null) {
				session.logout();
			}
		}
	}
}
