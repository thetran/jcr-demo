package com.pcloud.jcr_demo.builder;

/**
 * 
 * @author thetran
 *
 */
public class PathParser {
	public NodeData parse(String path) throws InvalidPathException {
		// TODO Validate the path
		NodeData data = new NodeData();
		
		int i = path.indexOf('@');
		
		
		
		if (i < 0) {
			String[] paths = path.replaceFirst("^/", "").split("/");
			data.setPath(paths);
		} else {
			
			String sub = path.substring(i);
			path = path.substring(0, i);
			
			String[] paths = path.replaceFirst("^/", "").split("/");
			data.setPath(paths);
			
			String[] pairs = sub.replaceFirst("^@", "").split("@");
			for (String pair : pairs) {
				String[] kv = pair.split("=");
				if (kv.length != 2) {
					throw new InvalidPathException("The path " + path + " contains invalid property: " + pair);
				}
				data.getProps().put(kv[0], kv[1]);
			}
		}
		return data;
	}
}
