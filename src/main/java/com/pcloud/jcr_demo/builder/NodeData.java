/**
 * 
 */
package com.pcloud.jcr_demo.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author thetran
 *
 */
public class NodeData {
	private String[] paths = {};
	private Map<String, String> props = new HashMap<String, String>();

	public String[] getPaths() {
		return paths;
	}

	public void setPath(String[] paths) {
		this.paths = paths;
	}

	public Map<String, String> getProps() {
		return props;
	}

	public void setProps(Map<String, String> props) {
		this.props = props;
	}

}
