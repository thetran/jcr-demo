package com.pcloud.jcr_demo.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Contains paths to all nodes and their properties.<br/>
 * Example: <br/>
 * The path is in form <code>/a/b/c<code/> <br/>
 * The property of a node is in form <code>/a/b/c@prop1=prop1Value@prop2=prop2Value<code/>
 * 
 * @author thetran
 *
 */
public class TreeData {
	private List<String> paths = new ArrayList<String>();

	public List<String> getPaths() {
		return paths;
	}

	public TreeData(String[] data) {
		paths.addAll(Arrays.asList(data));
	}

	public TreeData(List<String> data) {
		paths.addAll(data);
	}
}
