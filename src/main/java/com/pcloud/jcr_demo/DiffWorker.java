package com.pcloud.jcr_demo;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Credentials;
import javax.jcr.LoginException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

/**
 * 
 * @author thetran
 *
 */
public class DiffWorker {
	private Repository repo;
	private Credentials credential = new SimpleCredentials("pcloud", "password".toCharArray());
	private static Logger logger = Logger.getLogger(DiffWorker.class);
	
	public DiffWorker(Repository repo) {
		super();
		this.repo = repo;
	}

	/**
	 * Calculates diff between 2 workspaces
	 * 
	 * @param lhsWorkspace
	 * @param rhsWorkspace
	 * @return the {@link TreeDiff}
	 * @throws RepositoryException 
	 * @throws NoSuchWorkspaceException 
	 * @throws LoginException 
	 */
	public TreeDiff calculate(String lhsWorkspace, String rhsWorkspace) throws LoginException, NoSuchWorkspaceException, RepositoryException {
		List<String> lhsPaths = getTreeNodes(lhsWorkspace);
		List<String> rhsPaths = getTreeNodes(rhsWorkspace);
		
		Session lhsSession = repo.login(credential, lhsWorkspace);
		Session rhsSession = repo.login(credential, rhsWorkspace);
		
		TreeDiff td = compare(lhsSession, rhsSession, lhsPaths, rhsPaths);
		
		lhsSession.logout();
		rhsSession.logout();
		
		return td;
	}
	
	private static TreeDiff compare(Session lhsSession, Session rhsSession, List<String> lhsPaths, List<String> rhsPaths) throws PathNotFoundException, RepositoryException {
		
		TreeDiff td = new TreeDiff();

		Map<String, Node> m1 = new HashMap<String, Node>();
		Map<String, Node> m2 = new HashMap<String, Node>();

		for (String path : lhsPaths) {
			m1.put(path, lhsSession.getNode(path));
		}
		for (String path : rhsPaths) {
			m2.put(path, rhsSession.getNode(path));
		}

		for (String path : lhsPaths) {
			if (m2.containsKey(path)) {
				// compare properties
				NodeDiff nd = compare(lhsSession.getNode(path), rhsSession.getNode(path));
				if (nd.hasChanges()) {
					td.getModified().put(path, nd);
				}
			} else {
				td.getLhsOnly().add(path);
			}
		}
		for (String path : rhsPaths) {
			if (m1.containsKey(path)) {
				// compare properties
				NodeDiff nd = compare(lhsSession.getNode(path), rhsSession.getNode(path));
				if (nd.hasChanges()) {
					td.getModified().put(path, nd);
				}
			} else {
				td.getRhsOnly().add(path);
			}
		}

		return td;
	}
	
	private static NodeDiff compare(Node lhs, Node rhs) throws RepositoryException {

		NodeDiff nd = new NodeDiff(lhs, rhs);

		Map<String, Property> lhsProps = new HashMap<String, Property>();
		Map<String, Property> rhsProps = new HashMap<String, Property>();

		PropertyIterator lhsIter = lhs.getProperties();
		while (lhsIter.hasNext()) {
			Property prop = lhsIter.nextProperty();
			lhsProps.put(prop.getName(), prop);
		}

		PropertyIterator rhsIter = rhs.getProperties();
		while (rhsIter.hasNext()) {
			Property prop = rhsIter.nextProperty();
			rhsProps.put(prop.getName(), prop);
		}

		for (String prop : lhsProps.keySet()) {
			if (rhsProps.containsKey(prop)) {
				// compare values
				Property lhsProp = lhsProps.get(prop);
				Property rhsProp = rhsProps.get(prop);
				
				Object rhsValue = rhsProp.getString();
				Object lhsValue = lhsProp.getString();
				if (!Utils.isEqual(lhsValue, rhsValue)) {
					nd.getModifiedProps().put(prop,
							new ValueChange(lhsProp, rhsProp, lhsValue, rhsValue));
				}
			} else {
				nd.getLhsProps().add(prop);
			}
		}

		for (String prop : rhsProps.keySet()) {
			if (lhsProps.containsKey(prop)) {
				// compare values
				Property lhsProp = lhsProps.get(prop);
				Property rhsProp = rhsProps.get(prop);
				
				Object rhsValue = rhsProp.getString();
				Object lhsValue = lhsProp.getString();
				if (!Utils.isEqual(lhsValue, rhsValue)) {
					nd.getModifiedProps().put(prop,
							new ValueChange(lhsProp, rhsProp, lhsValue, rhsValue));
				}
			} else {
				nd.getRhsProps().add(prop);
			}
		}
		return nd;
	}
	
	private List<String> getTreeNodes(String ws) throws LoginException, NoSuchWorkspaceException, RepositoryException {
		List<String> paths = new ArrayList<String>();
		Session session = repo.login(credential, ws);
		Node root = session.getRootNode();
		logger.info("#### Tree: " + ws);
		Utils.dump(root, paths);
		session.logout();
		return paths;
	}
}
