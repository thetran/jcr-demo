package com.pcloud.jcr_demo;

import java.util.Set;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.jackrabbit.core.TransientRepository;
import org.apache.log4j.Logger;

import com.pcloud.jcr_demo.builder.InvalidPathException;
import com.pcloud.jcr_demo.builder.TreeBuilder;
import com.pcloud.jcr_demo.builder.TreeData;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    public static final String T1 = "a";
    public static final String T2 = "b";
    public static Logger log = Logger.getLogger(AppTest.class);
    private Repository repository;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void setUp() throws Exception {
        super.setUp();
        repository = new TransientRepository();
    }

    public void test2TreesHaveDifferentProps() {


        TreeBuilder tb =  new TreeBuilder(repository);
        TreeData data1 = new TreeData(new String[] { "/a/b/c/d@p1=hello@p2=same@p3=apple", "/a/e", "/a/i@p1=mouse", "/a/j" });
        TreeData data2 = new TreeData(new String[] { "/a/b/c/d@p1=world@p2=same@p4=banana", "/a/f/g", "/a/i@p1=mice" });

        try {
            tb.build(T1, data1);
            tb.build(T2, data2);

            DiffWorker dw = new DiffWorker(repository);
            TreeDiff td = dw.calculate(T1, T2);

            assertTrue(td.getLhsOnly().size() == 2);
            assertTrue(td.getLhsOnly().contains("/a/e"));
            assertTrue(td.getLhsOnly().contains("/a/j"));
            
            assertTrue(td.getRhsOnly().size() == 2);
            assertTrue(td.getRhsOnly().contains("/a/f"));
            assertTrue(td.getRhsOnly().contains("/a/f/g"));
            
            Set<String> keySet = td.getModified().keySet();
            assertTrue(keySet.size() == 2);
            assertTrue(keySet.contains("/a/b/c/d"));
            assertTrue(keySet.contains("/a/i"));

            NodeDiff nd = td.getModified().get("/a/b/c/d");
            
            assertTrue(nd.getLhsProps().size() == 1);
            nd.getLhsProps().contains("p3");
            
            assertTrue(nd.getRhsProps().size() == 1);
            nd.getRhsProps().contains("p4");
            
            assertTrue(nd.getModifiedProps().size() == 1);
            assertTrue(nd.getModifiedProps().containsKey("p1"));
            ValueChange vc = nd.getModifiedProps().get("p1");
            assertEquals("hello", vc.getLhsValue());
            assertEquals("world", vc.getRhsValue());
            
            nd = td.getModified().get("/a/i");
            
            assertTrue(nd.getLhsProps().isEmpty());
            assertTrue(nd.getRhsProps().isEmpty());
            assertTrue(nd.getModifiedProps().size() == 1);
            assertTrue(nd.getModifiedProps().containsKey("p1"));
            vc = nd.getModifiedProps().get("p1");
            assertEquals("mouse", vc.getLhsValue());
            assertEquals("mice", vc.getRhsValue());
        } catch (InvalidPathException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
    
    public void test2TreesHaveDifferentNodes3() {


        TreeBuilder tb =  new TreeBuilder(repository);
        TreeData data1 = new TreeData(new String[] { "/a/b/c/d", "/a/e", "/a/i", "/a/j" });
        TreeData data2 = new TreeData(new String[] { "/a/b/c/d", "/a/f/g", "/a/i" });

        try {
            tb.build(T1, data1);
            tb.build(T2, data2);

            DiffWorker dw = new DiffWorker(repository);
            TreeDiff td = dw.calculate(T1, T2);

            assertTrue(td.getLhsOnly().size() == 2);
            assertTrue(td.getLhsOnly().contains("/a/e"));
            assertTrue(td.getLhsOnly().contains("/a/j"));
            
            assertTrue(td.getRhsOnly().size() == 2);
            assertTrue(td.getRhsOnly().contains("/a/f"));
            assertTrue(td.getRhsOnly().contains("/a/f/g"));
            
            assertTrue(td.getModified().isEmpty());

        } catch (InvalidPathException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
    
    public void test2TreesHaveDifferentNodes2() {


        TreeBuilder tb =  new TreeBuilder(repository);
        TreeData data1 = new TreeData(new String[] { "/z/b/c/d", "/a/b/c/e/f" });
        TreeData data2 = new TreeData(new String[] { "/z/t/c/d", "/a/b/c/e/g" });

        try {
            tb.build(T1, data1);
            tb.build(T2, data2);

            DiffWorker dw = new DiffWorker(repository);
            TreeDiff td = dw.calculate(T1, T2);

            assertTrue(td.getLhsOnly().size() == 4);
            assertTrue(td.getLhsOnly().contains("/a/b/c/e/f"));
            assertTrue(td.getLhsOnly().contains("/z/b"));
            assertTrue(td.getLhsOnly().contains("/z/b/c"));
            assertTrue(td.getLhsOnly().contains("/z/b/c/d"));
            
            assertTrue(td.getRhsOnly().size() == 4);
            assertTrue(td.getRhsOnly().contains("/a/b/c/e/g"));
            assertTrue(td.getRhsOnly().contains("/z/t"));
            assertTrue(td.getRhsOnly().contains("/z/t/c"));
            assertTrue(td.getRhsOnly().contains("/z/t/c/d"));
            
            assertTrue(td.getModified().isEmpty());

        } catch (InvalidPathException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
    
    public void test2TreesHaveDifferentNodes() {


        TreeBuilder tb =  new TreeBuilder(repository);
        TreeData data1 = new TreeData(new String[] { "/a/b/c/d", "/a/b/c/e/f" });
        TreeData data2 = new TreeData(new String[] { "/a/b/c/d", "/a/b/c/e/g" });

        try {
            tb.build(T1, data1);
            tb.build(T2, data2);

            DiffWorker dw = new DiffWorker(repository);
            TreeDiff td = dw.calculate(T1, T2);

            assertTrue(td.getLhsOnly().size() == 1);
            assertTrue(td.getLhsOnly().contains("/a/b/c/e/f"));
            assertTrue(td.getRhsOnly().size() == 1);
            assertTrue(td.getRhsOnly().contains("/a/b/c/e/g"));
            assertTrue(td.getModified().isEmpty());

        } catch (InvalidPathException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }

    public void test2TreesAreEqual() {

        TreeBuilder tb =  new TreeBuilder(repository);
        TreeData data1 = new TreeData(new String[] { "/a/b/c/d", "/a/b/c/e", "/a@p1=aaa@p2=bbb", "/a/b/c@p4=ddd@p999=zzz@p3=ccc" });
        TreeData data2 = new TreeData(new String[] { "/a/b/c/d", "/a/b/c/e", "/a@p2=bbb@p1=aaa", "/a/b/c@p999=zzz@p3=ccc@p4=ddd" });
        
        try {
            tb.build(T1, data1);
            tb.build(T2, data2);

            DiffWorker dw = new DiffWorker(repository);
            TreeDiff td = dw.calculate(T1, T2);

            assertTrue(td.getLhsOnly().isEmpty());
            assertTrue(td.getRhsOnly().isEmpty());
            assertTrue(td.getModified().isEmpty());

        } catch (InvalidPathException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
}
